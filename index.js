let animado = document.querySelectorAll(".b-animado__img");

function mostrarScroll () {
    let scrollTop = document.documentElement.scrollTop;
    for(var i = 0; i < animado.length; i++) {
        let alturaAnimado = animado[i].offsetTop;
        if(alturaAnimado - 500 < scrollTop) {
            animado[i].style.opacity = 1;
            animado[i].classList.add("b-showRight")
        }

    }
}

let animadoText = document.querySelectorAll(".b-animado__text");

function mostrarScrollText () {
    let scrollTop = document.documentElement.scrollTop;
    for(var index = 0; index < animadoText.length; index++) {
        let alturaAnimadoText = animadoText[index].offsetTop;
        if(alturaAnimadoText - 500 < scrollTop) {
            animadoText[index].style.opacity = 1;
            animadoText[index].classList.add("b-showLeft")
        }
    }
}


window.addEventListener('scroll', mostrarScroll);
window.addEventListener('scroll', mostrarScrollText)